﻿using UnityEngine;
using System.Collections;

public class levelController : MonoBehaviour {
	public static int currentLevelNumber;
	public static int currentSectionNumber;
	public static string gameObjectiveTitle;
	public static ArrayList sectionTitles = new ArrayList();

	
	// Use this for initialization
	void Start () {
		setSectionTitles ();
		//PlayerPrefs.SetInt("PlayerSection", 10);
		//PlayerPrefs.Save ();
		//GameController.saveData ("PlayerSection1",1);
		//double[][] x = new double[5][];
		//x[0] = "sss";
		/*ArrayList myArrayList = new ArrayList();
		ArrayList levels = new ArrayList();
		levels.Add (1);
		levels.Add ("serhat");
		myArrayList.Add(levels);
		ArrayList userLevel = myArrayList [0];
		Debug.Log (userLevel[0]);*/
	}

	void Update () {
		if (islevelObjectiveMet() == true) {
				
		}
		Debug.Log(getGameObjectiveTitle ());
	}

	public static void setCurrentSection (int sectionNumber){
		currentSectionNumber = sectionNumber; 
	}

	public static void setCurrentLevel (int levelNumber){
		currentLevelNumber = levelNumber;
	}

	public static int getCurrentSectionNumber() {
		return currentSectionNumber;
	}
	public static int getCurrentLevelNumber() {
		return currentLevelNumber;
	}

	public static void setGameObjective() {
		int levelNumber = getCurrentLevelNumber();
		int sectionNumber = getCurrentSectionNumber ();

		if (sectionNumber == 1) {
			if (levelNumber == 1) {
				setGameObjectiveTitle("Make the goal. You get one shot.");
				setGoalBlocker(false);
			} else if (levelNumber == 2) {
				setGameObjectiveTitle("Pass between any two balls and score a goal");
				setGoalBlocker(true);
				GameController.setshowMoveCount(true);
			} else if (levelNumber == 3) {
				setGameObjectiveTitle("Pass between any two balls 3 times and score a goal");
				setGoalBlocker(true);
				GameController.setshowMoveCount(true);
			}
		}
		menuScript.setShowGameObjectiveGUI (true);

	}

	public static bool islevelObjectiveMet (){
		int levelNumber = getCurrentLevelNumber();
		int sectionNumber = getCurrentSectionNumber ();

		if (sectionNumber == 1) {
			if (levelNumber == 1) {

				return true;
			} else if (levelNumber == 2) {
				if (GameController.getTurnCount() == 1) {
					setGoalBlocker(false);
				}
			} else if (levelNumber == 3) {
				if (GameController.getTurnCount() == 3) {
					setGoalBlocker(false);
				}
			}
			return false;
		}
		return false;
	}
	public static void setGoalBlocker(bool isBlock) {
		GameObject goalBlocker;
		goalBlocker = GameObject.Find ("goalblocker");
		if (goalBlocker) {
			goalBlocker.SetActive (isBlock);
		}
	}
	public static ArrayList setSectionTitles (){
		sectionTitles.Add ("Let's get going !!!");
		sectionTitles.Add ("Hard on beach");

		return sectionTitles;
	}
	public static string getSectionTitles (int sectionNumber){
		return (string)sectionTitles[sectionNumber];
	}

	public static void setGameObjectiveTitle(string levelTitle) {
		gameObjectiveTitle = levelTitle;
	}
	public static string getGameObjectiveTitle() {
		return gameObjectiveTitle;
	}



}
