﻿using UnityEngine;
using System.Collections;

public class CBallSwipe : MonoBehaviour {
	Vector3 startPosition;
	Vector3 deltaPosition;
	LayerMask layerMask = ~( (1 << 8) | (1 << 9) | (1 << 10) | (1 << 11) | (1 << 12) | (1 << 13));
	string activeBallTag;

	GameObject ball1;
	GameObject ball2;
	GameObject ball3;
	Vector2 vball1;
	Vector2 vball2;
	Vector2 vball3;

	void Start() {
		setGameObjects ();
		GameController.clearTurnCount ();
		GameController.setMoveSuccess(true);

		int levelNumber = levelController.getCurrentLevelNumber ();
		if (levelNumber == 0) {
			levelController.setCurrentLevel (1);
		}
		int sectionNumber = levelController.getCurrentSectionNumber ();
		if (sectionNumber == 0) {
			levelController.setCurrentSection (1);
		}
		levelController.setGameObjective ();
	}
	void Update() {

		updateBallPositions ();
		//Debug.Log (activeBallTag);
		if (activeBallTag == "ball1") {
			Debug.DrawLine(vball2,vball3);
			if (GameController.getTurnMoveFlag() == false && Physics2D.Linecast (vball2, vball3, layerMask)) {
				//Debug.Log("innnn1");
				GameController.setTurnCount();
				GameController.setTurnMoveFlag(true);
				GameController.setMoveSuccess(true);
			}

		} else if (activeBallTag == "ball2") {
			if (GameController.getTurnMoveFlag() == false && Physics2D.Linecast (vball1, vball3, layerMask)) {		
				//Debug.Log("innnn2");
				GameController.setTurnCount();
				GameController.setTurnMoveFlag(true);
				GameController.setMoveSuccess(true);
			}
		} else if (activeBallTag == "ball3") {
			if (GameController.getTurnMoveFlag() == false && Physics2D.Linecast (vball1, vball2, layerMask)) {
				//Debug.Log("innnn3");
				GameController.setTurnCount();
				GameController.setTurnMoveFlag(true);
				GameController.setMoveSuccess(true);

			}
		}
		isBallStopped ();
		//Debug.Log ("moveSuccess:"+GameController.getMoveSuccess()+"activeBall:"+activeBallTag+"isBallStopped"+isBallStopped()+"menu gui"+menuScript.getShowGoalMenuGUI());
		if (!GameController.getMoveSuccess() && activeBallTag == null && isBallStopped() && menuScript.getShowGoalMenuGUI() == false) {
			Application.LoadLevel ("GameOver");
		}


	}
	private void OnMouseDown() {
		startPosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		layerMask = ~( (1 << 8) | (1 << 9) | (1 << 10));
	}
	private void OnMouseUp() {
		deltaPosition = Camera.main.ScreenToWorldPoint (Input.mousePosition) - startPosition;

		if (isBallOnTheMove() == false) {
			GameController.setTurnMoveFlag(false);
			GameController.setMoveSuccess(false);
			rigidbody2D.AddForce(new Vector2(-deltaPosition.x*100,-deltaPosition.y*100));
			GameController.isGameStarted = true;
		}

		activeBallTag = rigidbody2D.tag;
		if ( activeBallTag == "ball1") {
			layerMask = ~((1 << 8) | (1 << 9) | (1 << 10) | (1 << 12) | (1 << 13));		
		} else if (activeBallTag == "ball2") {
			layerMask = ~((1 << 8) | (1 << 9) | (1 << 10) | (1 << 11) | (1 << 13));		
		} else if (activeBallTag == "ball3") {
			layerMask = ~((1 << 8) | (1 << 9) | (1 << 10) | (1 << 11) | (1 << 12));		
		}
	}

	public bool isBallStopped() {
		if (!ball1.gameObject.rigidbody2D.IsSleeping() || !ball2.gameObject.rigidbody2D.IsSleeping() || !ball3.gameObject.rigidbody2D.IsSleeping()) 
		{
			return false;
		} else {
			activeBallTag = null;
			return true;
		}
	}
	public bool isBallOnTheMove() {
		if (ball1.gameObject.rigidbody2D.IsSleeping() && ball2.gameObject.rigidbody2D.IsSleeping() && ball3.gameObject.rigidbody2D.IsSleeping()) 
		{

			//GameController.setMoveStatus(false);
			return false;
		} else {
			return true;
		}
	}
	public void OnTriggerEnter2D (Collider2D collider) {
		if (collider.gameObject.tag == "goalline") {

			GameObject goalSound = GameObject.Find ("goal");
			goalSound.audio.Play ();
			StartCoroutine(GoalWait());
		}
	}

	IEnumerator GoalWait() {
		yield return new WaitForSeconds(1);
		menuScript.setShowGoalMenuGUI(true);
	}


	public void setGameObjects() {
		ball1 = GameObject.Find ("Soccerball1");
		ball2 = GameObject.Find ("Soccerball2");
		ball3 = GameObject.Find ("Soccerball3");
	}
	public void updateBallPositions() {
		vball1 = new Vector2 (ball1.transform.position.x,ball1.transform.position.y);
		vball2 = new Vector2 (ball2.transform.position.x,ball2.transform.position.y);
		vball3 = new Vector2 (ball3.transform.position.x,ball3.transform.position.y);
	}
	/*void OnCollisionEnter2D(Collision2D coll) {
		Debug.Log ("dasdasda");
	}*/
}