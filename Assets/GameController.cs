﻿using UnityEngine;
using System.Collections;


public class GameController : MonoBehaviour {
	public static int moveTurnCount = 0;
	public static bool isGameStarted = false;
	public static bool moveSuccess = false;
	public static bool turnMoveFlag = false;
	public static bool showMoveCount = false;

	
	// Use this for initialization

	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (getShowMoveCount()){
			guiText.text = getTurnCount().ToString();
		}
		//Debug.Log (moveTurnCount	);
	}


	public static void setTurnCount() {
		moveTurnCount = moveTurnCount + 1;
	}
	public static int getTurnCount() {
		return moveTurnCount;
	}
	public static void clearTurnCount() {
		moveTurnCount = 0;
	}
	// move count holder prevents multiple move count (collider)
	public static bool getTurnMoveFlag() {
		return turnMoveFlag;
	}
	public static void setTurnMoveFlag(bool started) {
		turnMoveFlag = started;
	}
	public static void setMoveSuccess(bool isSuccess) {
		moveSuccess = isSuccess;
	}
	public static bool getMoveSuccess() {
		return moveSuccess;
	}
	public static void setshowMoveCount(bool showCountFlag) {
		showMoveCount = showCountFlag;
	}
	public static bool getShowMoveCount() {
		return showMoveCount;
	}

	public static void saveData (string key, int value) {
		PlayerPrefs.SetInt (key, value);
	}
	public static int getSaveData (string key) {
		return PlayerPrefs.GetInt (key);
	}



}
