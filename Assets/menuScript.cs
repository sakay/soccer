﻿using UnityEngine;
using System.Collections;

public class menuScript : MonoBehaviour {

	public static bool ShowGoalMenuGUI = false;
	public static bool ShowGameObjectiveGUI = false;

	// Use this for initialization
	void Start () {

	}

	void OnGUI () {

		if (ShowGoalMenuGUI) {
			// Make a background box
			GUI.Box(new Rect(200f,100f,200,200), "GOALLL!!!");

			if(GUI.Button(new Rect(260f,130f,80f,50f), "Next Level")) {
				setShowGoalMenuGUI(false);
				Application.LoadLevel ("iosScene");
				int currentLevel = levelController.getCurrentLevelNumber();
				int currentSection = levelController.getCurrentSectionNumber();
				levelController.setCurrentLevel(currentLevel + 1);
				levelController.setCurrentSection(currentSection);
			}

			if(GUI.Button(new Rect(260f,190f,80f,50f), "Redo Level")) {
				setShowGoalMenuGUI(false);
				//Application.LoadLevel ("iosScene");
			}
		}
		if (ShowGameObjectiveGUI) {
			GUI.Box(new Rect(120f,100f,400,100), levelController.getGameObjectiveTitle());
			
			if(GUI.Button(new Rect(260f,130f,100f,50f), "OK, I got it !!!")) {
				setShowGameObjectiveGUI(false);
			}
		}
	}

	public static void setShowGoalMenuGUI(bool boolFlag) {
		ShowGoalMenuGUI = boolFlag;
	}

	public static bool getShowGoalMenuGUI() {
		return ShowGoalMenuGUI;
	}

	public static void setShowGameObjectiveGUI(bool boolFlag) {
		ShowGameObjectiveGUI = boolFlag;
	}
	
	public static bool getShowGameObjectiveGUI() {
		return ShowGameObjectiveGUI;
	}

	
	// Update is called once per frame
	void Update () {
		
	}
	
	private void OnMouseDown() {
		Application.LoadLevel ("iosScene");
	}
}
